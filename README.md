Ocf
===

OCaml library to read and write configuration files in JSON syntax.

See [https://zoggy.frama.io/ocf](https://zoggy.frama.io/ocf) for more
information.

### Installation

````
./configure
make all install
````

This installs the `ocf` package.


